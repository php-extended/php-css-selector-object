# php-extended/php-css-selector-object

A library that implements the php-extended/php-css-selector-interface interface library.

![coverage](https://gitlab.com/php-extended/php-css-selector-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-css-selector-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-css-selector-object ^8`


## Basic Usage

To build a css-selector tree, just use the collection node and the single nodes :

```php

use PhpExtended\Css\CssAndSelector;
use PhpExtended\Css\CssOrSelector;
use PhpExtended\Css\CssNotSelector;
use PhpExtended\Css\CssAttributeEqualsSelector;
use PhpExtended\Css\CssElementSelector;
use PhpExtended\Css\CssStateSelector;

$s = new CssElementSelector('p'); // p
$s = new CssAndSelector($s, new CssAttributeEqualsSelector('class', 'foo')); // p.foo
$s2 = new CssAttributeEqualsSelector('id', 'bar'); // #bar
$s2 = new CssAndSelector($s2, new CssAttributeEqualsSelector('data-target', 'foobar')); // #bar[data-target=foobar]
$s = new CssNotSelector($s, new CssAttributeEqualsSelector('class', 'boo')); // p.foo:not(.boo)
$s = new CssOrSelector($s, $s2); // p.foo:not(.boo), #bar[data-target=foobar]

```

To parse css selector data, do the following :

```php

use PhpExtended\Css\CssSelectorParser;

$parser = new CssSelectorParser();
$node = $parser->parse('div p:not(.foo)');
// $node is now a CssAbstractSelectorInterface

```


## License

MIT (See [license file](LICENSE)).
