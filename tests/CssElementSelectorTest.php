<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssElementSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssElementSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssElementSelector
 *
 * @internal
 *
 * @small
 */
class CssElementSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssElementSelector
	 */
	protected CssElementSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssElementSelector('name');
	}
	
}
