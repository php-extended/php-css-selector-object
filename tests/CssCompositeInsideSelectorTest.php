<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssCompositeInsideSelector;
use PhpExtended\Css\CssElementSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssCompositeInsideSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssCompositeInsideSelector
 *
 * @internal
 *
 * @small
 */
class CssCompositeInsideSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssCompositeInsideSelector
	 */
	protected CssCompositeInsideSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name1>name2', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssCompositeInsideSelector(
			new CssElementSelector('name1'),
			new CssElementSelector('name2'),
		);
	}
	
}
