<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAbstractSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAbstractSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAbstractSelector
 *
 * @internal
 *
 * @small
 */
class CssAbstractSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAbstractSelector
	 */
	protected CssAbstractSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(CssAbstractSelector::class, ['name']);
	}
	
}
