<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAttributeEndsSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAttributeEndsSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAttributeEndsSelector
 *
 * @internal
 *
 * @small
 */
class CssAttributeEndsSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAttributeEndsSelector
	 */
	protected CssAttributeEndsSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[name$=value]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssAttributeEndsSelector('name', 'value');
	}
	
}
