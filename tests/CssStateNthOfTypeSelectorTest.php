<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateNthOfTypeSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateNthOfTypeSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateNthOfTypeSelector
 *
 * @internal
 *
 * @small
 */
class CssStateNthOfTypeSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateNthOfTypeSelector
	 */
	protected CssStateNthOfTypeSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':nth-of-type(9)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateNthOfTypeSelector(9);
	}
	
}
