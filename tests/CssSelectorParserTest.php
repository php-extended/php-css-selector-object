<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAttributeBeginsSelector;
use PhpExtended\Css\CssAttributeContainsSelector;
use PhpExtended\Css\CssAttributeEndsSelector;
use PhpExtended\Css\CssAttributeEqualsSelector;
use PhpExtended\Css\CssAttributeExistsSelector;
use PhpExtended\Css\CssAttributeFirstSelector;
use PhpExtended\Css\CssAttributeWordSelector;
use PhpExtended\Css\CssCompositeAfterSelector;
use PhpExtended\Css\CssCompositeAndSelector;
use PhpExtended\Css\CssCompositeBeforeSelector;
use PhpExtended\Css\CssCompositeDescendantSelector;
use PhpExtended\Css\CssCompositeInsideSelector;
use PhpExtended\Css\CssCompositeOrSelector;
use PhpExtended\Css\CssElementSelector;
use PhpExtended\Css\CssSelectorParser;
use PhpExtended\Css\CssStateActiveSelector;
use PhpExtended\Css\CssStateAfterSelector;
use PhpExtended\Css\CssStateBeforeSelector;
use PhpExtended\Css\CssStateCheckedSelector;
use PhpExtended\Css\CssStateDefaultSelector;
use PhpExtended\Css\CssStateDisabledSelector;
use PhpExtended\Css\CssStateEmptySelector;
use PhpExtended\Css\CssStateEnabledSelector;
use PhpExtended\Css\CssStateFirstChildSelector;
use PhpExtended\Css\CssStateFirstLetterSelector;
use PhpExtended\Css\CssStateFirstLineSelector;
use PhpExtended\Css\CssStateFirstOfTypeSelector;
use PhpExtended\Css\CssStateFocusSelector;
use PhpExtended\Css\CssStateHoverSelector;
use PhpExtended\Css\CssStateIndeterminateSelector;
use PhpExtended\Css\CssStateInRangeSelector;
use PhpExtended\Css\CssStateInvalidSelector;
use PhpExtended\Css\CssStateLastChildSelector;
use PhpExtended\Css\CssStateLastOfTypeSelector;
use PhpExtended\Css\CssStateLinkSelector;
use PhpExtended\Css\CssStateNthChildSelector;
use PhpExtended\Css\CssStateNthLastChildSelector;
use PhpExtended\Css\CssStateNthLastOfTypeSelector;
use PhpExtended\Css\CssStateNthOfTypeSelector;
use PhpExtended\Css\CssStateOnlyChildSelector;
use PhpExtended\Css\CssStateOnlyOfTypeSelector;
use PhpExtended\Css\CssStateOptionalSelector;
use PhpExtended\Css\CssStateOutOfRangeSelector;
use PhpExtended\Css\CssStatePlaceholderSelector;
use PhpExtended\Css\CssStateReadOnlySelector;
use PhpExtended\Css\CssStateReadWriteSelector;
use PhpExtended\Css\CssStateRequiredSelector;
use PhpExtended\Css\CssStateRootSelector;
use PhpExtended\Css\CssStateSelectionSelector;
use PhpExtended\Css\CssStateTargetSelector;
use PhpExtended\Css\CssStateValidSelector;
use PhpExtended\Css\CssStateVisitedSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssSelectorParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssSelectorParser
 *
 * @internal
 *
 * @small
 */
class CssSelectorParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var CssSelectorParser
	 */
	protected CssSelectorParser $_parser;
	
	public function testEmpty() : void
	{
		$this->assertEquals(new CssElementSelector('*'), $this->_parser->parse(null));
	}
	
	public function testAll() : void
	{
		$this->assertEquals(new CssElementSelector('*'), $this->_parser->parse('*'));
	}
	
	public function testElement() : void
	{
		$this->assertEquals(new CssElementSelector('elem'), $this->_parser->parse('elem'));
	}
	
	public function testClass() : void
	{
		$this->assertEquals(new CssAttributeWordSelector('class', 'row'), $this->_parser->parse('.row'));
	}
	
	public function testId() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('id', 'bar'), $this->_parser->parse('#bar'));
	}
	
	public function testAnd() : void
	{
		$this->assertEquals(new CssCompositeAndSelector(
			new CssAttributeWordSelector('class', 'foo'),
			new CssAttributeWordSelector('class', 'bar'),
		), $this->_parser->parse('.foo.bar'));
	}
	
	public function testElementClass() : void
	{
		$this->assertEquals(new CssCompositeAndSelector(
			new CssElementSelector('p'),
			new CssAttributeWordSelector('class', 'intro'),
		), $this->_parser->parse('p.intro'));
	}
	
	public function testElementOr() : void
	{
		$this->assertEquals(new CssCompositeOrSelector(
			new CssElementSelector('div'),
			new CssElementSelector('p'),
		), $this->_parser->parse('div , p'));
	}
	
	public function testElementDescendant() : void
	{
		$this->assertEquals(new CssCompositeDescendantSelector(
			new CssElementSelector('div'),
			new CssElementSelector('p'),
		), $this->_parser->parse('div p'));
	}
	
	public function testElementChildren() : void
	{
		$this->assertEquals(new CssCompositeInsideSelector(
			new CssElementSelector('div'),
			new CssElementSelector('p'),
		), $this->_parser->parse('div > p'));
	}
	
	public function testElementAfter() : void
	{
		$this->assertEquals(new CssCompositeAfterSelector(
			new CssElementSelector('div'),
			new CssElementSelector('p'),
		), $this->_parser->parse('div + p'));
	}
	
	public function testElementBefore() : void
	{
		$this->assertEquals(new CssCompositeBeforeSelector(
			new CssElementSelector('ul'),
			new CssElementSelector('p'),
		), $this->_parser->parse('ul ~ p'));
	}
	
	public function testAttributeEmpty() : void
	{
		$this->assertEquals(new CssAttributeExistsSelector('*'), $this->_parser->parse('[]'));
	}
	
	public function testAttributeExists() : void
	{
		$this->assertEquals(new CssAttributeExistsSelector('attr'), $this->_parser->parse('[attr]'));
	}
	
	public function testAttributeEquals() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('target', '_blank'), $this->_parser->parse('[target=_blank]'));
	}
	
	public function testAttributeWord() : void
	{
		$this->assertEquals(new CssAttributeWordSelector('title', 'flower'), $this->_parser->parse('[title~=flower]'));
	}
	
	public function testAttributeStartsWith() : void
	{
		$this->assertEquals(new CssAttributeFirstSelector('lang', 'en'), $this->_parser->parse('[lang|=en]'));
	}
	
	public function testAttributeBeginsWith() : void
	{
		$this->assertEquals(new CssAttributeBeginsSelector('href', 'https'), $this->_parser->parse('[href^=https]'));
	}
	
	public function testAttributeEndsWith() : void
	{
		$this->assertEquals(new CssAttributeEndsSelector('href', '.pdf'), $this->_parser->parse('[href$=.pdf]'));
	}
	
	public function testAttributeContains() : void
	{
		$this->assertEquals(new CssAttributeContainsSelector('href', 'css'), $this->_parser->parse('[href*=css]'));
	}
	
	public function testAttributeContainsEquals() : void
	{
		$this->assertEquals(new CssAttributeContainsSelector('href', 'foo=bar'), $this->_parser->parse('[href*=foo=bar]'));
	}
	
	public function testQuotedAttribute() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('href', 'foobar'), $this->_parser->parse('[href="foobar"]'));
	}
	
	public function testQuotedAttribute2() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('href', 'foo\\bar'), $this->_parser->parse('[href="foo\\bar"]'));
	}
	
	public function testAttributeFuncBegin() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('href', 'toto('), $this->_parser->parse('[href=toto(]'));
	}
	
	public function testStateActive() : void
	{
		$this->assertEquals(new CssStateActiveSelector(), $this->_parser->parse(':active'));
	}
	
	public function testStateAfter() : void
	{
		$this->assertEquals(new CssStateAfterSelector(), $this->_parser->parse('::after'));
	}
	
	public function testStateBefore() : void
	{
		$this->assertEquals(new CssStateBeforeSelector(), $this->_parser->parse('::before'));
	}
	
	public function testStateChecked() : void
	{
		$this->assertEquals(new CssStateCheckedSelector(), $this->_parser->parse(':checked'));
	}
	
	public function testStateDefault() : void
	{
		$this->assertEquals(new CssStateDefaultSelector(), $this->_parser->parse(':default'));
	}
	
	public function testStateDisabled() : void
	{
		$this->assertEquals(new CssStateDisabledSelector(), $this->_parser->parse(':disabled'));
	}
	
	public function testStateEmpty() : void
	{
		$this->assertEquals(new CssStateEmptySelector(), $this->_parser->parse(':empty'));
	}
	
	public function testStateEnabled() : void
	{
		$this->assertEquals(new CssStateEnabledSelector(), $this->_parser->parse(':enabled'));
	}
	
	public function testStateFirstChild() : void
	{
		$this->assertEquals(new CssStateFirstChildSelector(), $this->_parser->parse(':first-child'));
	}
	
	public function testStateFirstLetter() : void
	{
		$this->assertEquals(new CssStateFirstLetterSelector(), $this->_parser->parse('::first-letter'));
	}
	
	public function testStateFirstLine() : void
	{
		$this->assertEquals(new CssStateFirstLineSelector(), $this->_parser->parse('::first-line'));
	}
	
	public function testStateFirstOfType() : void
	{
		$this->assertEquals(new CssStateFirstOfTypeSelector(), $this->_parser->parse(':first-of-type'));
	}
	
	public function testStateFocus() : void
	{
		$this->assertEquals(new CssStateFocusSelector(), $this->_parser->parse(':focus'));
	}
	
	public function testStateHover() : void
	{
		$this->assertEquals(new CssStateHoverSelector(), $this->_parser->parse(':hover'));
	}
	
	public function testStateInRange() : void
	{
		$this->assertEquals(new CssStateInRangeSelector(), $this->_parser->parse(':in-range'));
	}
	
	public function testStateIndeterminate() : void
	{
		$this->assertEquals(new CssStateIndeterminateSelector(), $this->_parser->parse(':indeterminate'));
	}
	
	public function testStateInvalid() : void
	{
		$this->assertEquals(new CssStateInvalidSelector(), $this->_parser->parse(':invalid'));
	}
	
	public function testStateLangExists() : void
	{
		$this->assertEquals(new CssAttributeExistsSelector('lang'), $this->_parser->parse(':lang'));
	}
	
	public function testStateLang() : void
	{
		$this->assertEquals(new CssAttributeEqualsSelector('lang', 'en'), $this->_parser->parse('lang(en)'));
	}
	
	public function testStateLastChild() : void
	{
		$this->assertEquals(new CssStateLastChildSelector(), $this->_parser->parse(':last-child'));
	}
	
	public function testStateLastOfType() : void
	{
		$this->assertEquals(new CssStateLastOfTypeSelector(), $this->_parser->parse(':last-of-type'));
	}
	
	public function testStateLink() : void
	{
		$this->assertEquals(new CssStateLinkSelector(), $this->_parser->parse(':link'));
	}
	
	public function testStateNthChildExists() : void
	{
		$this->assertEquals(new CssStateNthChildSelector(1), $this->_parser->parse(':nth-child'));
	}
	
	public function testStateNthChild() : void
	{
		$this->assertEquals(new CssStateNthChildSelector(2), $this->_parser->parse(':nth-child(2)'));
	}
	
	public function testStateNthLastChildExists() : void
	{
		$this->assertEquals(new CssStateNthLastChildSelector(1), $this->_parser->parse(':nth-last-child'));
	}
	
	public function testStateNthLastChild() : void
	{
		$this->assertEquals(new CssStateNthLastChildSelector(3), $this->_parser->parse(':nth-last-child(3)'));
	}
	
	public function testStateNthLastOfTypeExists() : void
	{
		$this->assertEquals(new CssStateNthLastOfTypeSelector(1), $this->_parser->parse(':nth-last-of-type'));
	}
	
	public function testStateNthLastOfType() : void
	{
		$this->assertEquals(new CssStateNthLastOfTypeSelector(4), $this->_parser->parse(':nth-last-of-type(4)'));
	}
	
	public function testStateNthOfTypeExists() : void
	{
		$this->assertEquals(new CssStateNthOfTypeSelector(1), $this->_parser->parse(':nth-of-type'));
	}
	
	public function testStateNthOfType() : void
	{
		$this->assertEquals(new CssStateNthOfTypeSelector(5), $this->_parser->parse(':nth-of-type(5)'));
	}
	
	public function testStateOnlyOfType() : void
	{
		$this->assertEquals(new CssStateOnlyOfTypeSelector(), $this->_parser->parse(':only-of-type'));
	}
	
	public function testStateOnlyChild() : void
	{
		$this->assertEquals(new CssStateOnlyChildSelector(), $this->_parser->parse(':only-child'));
	}
	
	public function testStateOptional() : void
	{
		$this->assertEquals(new CssStateOptionalSelector(), $this->_parser->parse(':optional'));
	}
	
	public function testStateOutOfRange() : void
	{
		$this->assertEquals(new CssStateOutOfRangeSelector(), $this->_parser->parse(':out-of-range'));
	}
	
	public function testStatePlaceholder() : void
	{
		$this->assertEquals(new CssStatePlaceholderSelector(), $this->_parser->parse('::placeholder'));
	}
	
	public function testStateReadOnly() : void
	{
		$this->assertEquals(new CssStateReadOnlySelector(), $this->_parser->parse(':read-only'));
	}
	
	public function testStateReadWrite() : void
	{
		$this->assertEquals(new CssStateReadWriteSelector(), $this->_parser->parse(':read-write'));
	}
	
	public function testStateRequired() : void
	{
		$this->assertEquals(new CssStateRequiredSelector(), $this->_parser->parse(':required'));
	}
	
	public function testStateRoot() : void
	{
		$this->assertEquals(new CssStateRootSelector(), $this->_parser->parse(':root'));
	}
	
	public function testStateSelection() : void
	{
		$this->assertEquals(new CssStateSelectionSelector(), $this->_parser->parse('::selection'));
	}
	
	public function testStateTarget() : void
	{
		$this->assertEquals(new CssStateTargetSelector(), $this->_parser->parse(':target'));
	}
	
	public function testStateValid() : void
	{
		$this->assertEquals(new CssStateValidSelector(), $this->_parser->parse(':valid'));
	}
	
	public function testStateVisited() : void
	{
		$this->assertEquals(new CssStateVisitedSelector(), $this->_parser->parse(':visited'));
	}
	
	public function testStateNone() : void
	{
		$this->assertEquals(new CssElementSelector('*'), $this->_parser->parse(':foobar'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new CssSelectorParser();
	}
	
}
