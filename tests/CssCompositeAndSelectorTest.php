<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAttributeEqualsSelector;
use PhpExtended\Css\CssCompositeAndSelector;
use PhpExtended\Css\CssElementSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssCompositeAndSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssCompositeAndSelector
 *
 * @internal
 *
 * @small
 */
class CssCompositeAndSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssCompositeAndSelector
	 */
	protected CssCompositeAndSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name1#name2', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssCompositeAndSelector(
			new CssElementSelector('name1'),
			new CssAttributeEqualsSelector('id', 'name2'),
		);
	}
	
}
