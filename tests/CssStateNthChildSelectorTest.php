<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateNthChildSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateNthChildSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateNthChildSelector
 *
 * @internal
 *
 * @small
 */
class CssStateNthChildSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateNthChildSelector
	 */
	protected CssStateNthChildSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':nth-child(9)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateNthChildSelector(9);
	}
	
}
