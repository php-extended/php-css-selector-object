<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateSelectionSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateSelectionSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateSelectionSelector
 *
 * @internal
 *
 * @small
 */
class CssStateSelectionSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateSelectionSelector
	 */
	protected CssStateSelectionSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('::selection', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateSelectionSelector();
	}
	
}
