<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateActiveSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateActiveSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateActiveSelector
 *
 * @internal
 *
 * @small
 */
class CssStateActiveSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateActiveSelector
	 */
	protected CssStateActiveSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':active', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateActiveSelector();
	}
	
}
