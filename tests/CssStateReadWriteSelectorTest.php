<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateReadWriteSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateReadWriteSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateReadWriteSelector
 *
 * @internal
 *
 * @small
 */
class CssStateReadWriteSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateReadWriteSelector
	 */
	protected CssStateReadWriteSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':read-write', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateReadWriteSelector();
	}
	
}
