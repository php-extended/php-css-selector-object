<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateLinkSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateLinkSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateLinkSelector
 *
 * @internal
 *
 * @small
 */
class CssStateLinkSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateLinkSelector
	 */
	protected CssStateLinkSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':link', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateLinkSelector();
	}
	
}
