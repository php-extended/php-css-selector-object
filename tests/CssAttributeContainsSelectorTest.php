<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAttributeContainsSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAttributeContainsSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAttributeContainsSelector
 *
 * @internal
 *
 * @small
 */
class CssAttributeContainsSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAttributeContainsSelector
	 */
	protected CssAttributeContainsSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[name*=value]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssAttributeContainsSelector('name', 'value');
	}
	
}
