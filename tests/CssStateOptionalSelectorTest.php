<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateOptionalSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateOptionalSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateOptionalSelector
 *
 * @internal
 *
 * @small
 */
class CssStateOptionalSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateOptionalSelector
	 */
	protected CssStateOptionalSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':optional', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateOptionalSelector();
	}
	
}
