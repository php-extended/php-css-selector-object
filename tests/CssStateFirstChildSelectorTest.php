<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateFirstChildSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateFirstChildSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateFirstChildSelector
 *
 * @internal
 *
 * @small
 */
class CssStateFirstChildSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateFirstChildSelector
	 */
	protected CssStateFirstChildSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':first-child', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateFirstChildSelector();
	}
	
}
