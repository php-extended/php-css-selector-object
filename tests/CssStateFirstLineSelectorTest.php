<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateFirstLineSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateFirstLineSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateFirstLineSelector
 *
 * @internal
 *
 * @small
 */
class CssStateFirstLineSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateFirstLineSelector
	 */
	protected CssStateFirstLineSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('::first-line', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateFirstLineSelector();
	}
	
}
