<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateRequiredSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateRequiredSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateRequiredSelector
 *
 * @internal
 *
 * @small
 */
class CssStateRequiredSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateRequiredSelector
	 */
	protected CssStateRequiredSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':required', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateRequiredSelector();
	}
	
}
