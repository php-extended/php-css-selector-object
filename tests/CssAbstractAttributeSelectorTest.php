<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAbstractAttributeSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAbstractAttributeSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAbstractAttributeSelector
 *
 * @internal
 *
 * @small
 */
class CssAbstractAttributeSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAbstractAttributeSelector
	 */
	protected CssAbstractAttributeSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[nameOPRvalue]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $mock = $this->getMockForAbstractClass(CssAbstractAttributeSelector::class, ['name', 'value']);
		$mock->expects($this->any())
			->method('getOperator')
			->willReturn('OPR')
		;
	}
	
}
