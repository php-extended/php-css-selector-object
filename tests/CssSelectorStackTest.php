<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssCompositeAndSelector;
use PhpExtended\Css\CssElementSelector;
use PhpExtended\Css\CssSelectorStack;
use PHPUnit\Framework\TestCase;

/**
 * CssSelectorStackTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssSelectorStack
 *
 * @internal
 *
 * @small
 */
class CssSelectorStackTest extends TestCase
{
	
	/**
	 * The stack to test.
	 * 
	 * @var CssSelectorStack
	 */
	protected CssSelectorStack $_stack;
	
	public function testToString() : void
	{
		$object = $this->_stack;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testEmpty() : void
	{
		$this->assertEquals(new CssElementSelector('*'), $this->_stack->getCompact());
	}
	
	public function testAddNode() : void
	{
		$this->_stack->addNode(new CssElementSelector('toto'));
		$this->assertEquals(new CssElementSelector('toto'), $this->_stack->getCompact());
	}
	
	public function testAdd2Node() : void
	{
		$this->_stack->addNode(new CssElementSelector('toto'));
		$this->_stack->addNode(new CssElementSelector('tata'));
		$this->assertEquals(new CssCompositeAndSelector(
			new CssElementSelector('toto'),
			new CssElementSelector('tata'),
		), $this->_stack->getCompact());
	}
	
	public function testAddOp() : void
	{
		$this->_stack->addOperator(CssSelectorStack::OP_AND);
		$this->assertEquals(new CssElementSelector('*'), $this->_stack->getCompact());
	}
	
	public function testAdd2Op() : void
	{
		$this->_stack->addOperator(CssSelectorStack::OP_OR);
		$this->_stack->addOperator(CssSelectorStack::OP_AND);
		$this->assertEquals(new CssCompositeAndSelector(
			new CssElementSelector('*'),
			new CssElementSelector('*'),
		), $this->_stack->getCompact());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_stack = new CssSelectorStack();
	}
	
}
