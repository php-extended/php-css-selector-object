<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssCompositeNotSelector;
use PhpExtended\Css\CssElementSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssCompositeNotSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssCompositeNotSelector
 *
 * @internal
 *
 * @small
 */
class CssCompositeNotSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssCompositeNotSelector
	 */
	protected CssCompositeNotSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name1:not(name2)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssCompositeNotSelector(new CssElementSelector('name1'), new CssElementSelector('name2'));
	}
	
}
