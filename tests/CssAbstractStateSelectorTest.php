<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAbstractStateSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAbstractStateSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAbstractStateSelector
 *
 * @internal
 *
 * @small
 */
class CssAbstractStateSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAbstractStateSelector
	 */
	protected CssAbstractStateSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':name(9)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(CssAbstractStateSelector::class, ['name', 9]);
	}
	
}
