<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssAbstractCompositeSelector;
use PhpExtended\Css\CssElementSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssAbstractCompositeSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssAbstractCompositeSelector
 *
 * @internal
 *
 * @small
 */
class CssAbstractCompositeSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssAbstractCompositeSelector
	 */
	protected CssAbstractCompositeSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name1OPRname2', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $mock = $this->getMockForAbstractClass(CssAbstractCompositeSelector::class, ['name',
			new CssElementSelector('name1'),
			new CssElementSelector('name2'),
		]);
		$mock->expects($this->any())
			->method('getOperator')
			->willReturn('OPR')
		;
	}
	
}
