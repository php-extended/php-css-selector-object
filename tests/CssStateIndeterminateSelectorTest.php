<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateIndeterminateSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateIndeterminateSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateIndeterminateSelector
 *
 * @internal
 *
 * @small
 */
class CssStateIndeterminateSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateIndeterminateSelector
	 */
	protected CssStateIndeterminateSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':indeterminate', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateIndeterminateSelector();
	}
	
}
