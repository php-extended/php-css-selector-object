<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Css\CssStateBeforeSelector;
use PHPUnit\Framework\TestCase;

/**
 * CssStateBeforeSelectorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Css\CssStateBeforeSelector
 *
 * @internal
 *
 * @small
 */
class CssStateBeforeSelectorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CssStateBeforeSelector
	 */
	protected CssStateBeforeSelector $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(':before', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CssStateBeforeSelector();
	}
	
}
