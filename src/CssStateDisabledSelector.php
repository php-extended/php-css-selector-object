<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateDisabledSelector class file.
 * 
 * This class represents the :disabled pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateDisabledSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateDisabledSelector.
	 */
	public function __construct()
	{
		parent::__construct('disabled');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return $node->hasAttribute('disabled');
	}
	
}
