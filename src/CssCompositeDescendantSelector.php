<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use SplStack;

/**
 * CssCompositeDescendantSelector class file.
 * 
 * This class represents a composite selector that matches a node that is
 * inside another node, regardless of the depths.
 * 
 * @author Anastaszor
 */
class CssCompositeDescendantSelector extends CssAbstractCompositeSelector
{
	
	/**
	 * Builds a new CssCompositeDescendantSelector with both selectors.
	 * 
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct('DESC', $before, $after);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return ' '; // space
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if(null === $parentStack || $parentStack->isEmpty())
		{
			return false;
		}
		
		if(!$this->getAfterSelector()->matches($node, $parentStack))
		{
			return false;
		}
		
		/** @var SplStack<HtmlCollectionNodeInterface> $npstack */
		$npstack = new SplStack();
		
		foreach($parentStack as $parentNode)
		{
			if($this->getBeforeSelector()->matches($parentNode, $npstack))
			{
				return true;
			}
			$npstack->unshift($parentNode);
		}
		
		return false;
	}
	
}
