<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssCompositeNotSelector class file.
 * 
 * This class represents a composite selector that matches a node only if it
 * matches the selector before and not the selector after.
 * 
 * @author Anastaszor
 */
class CssCompositeNotSelector extends CssAbstractCompositeSelector
{
	
	/**
	 * Builds a new CssCompositeNotSelector with both selectors.
	 * 
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct('NOT', $before, $after);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::__toString()
	 */
	public function __toString() : string
	{
		return $this->_before->__toString().$this->getOperator().'('.$this->_after->__toString().')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return ':not';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return $this->getBeforeSelector()->matches($node, $parentStack)
			&& !$this->getAfterSelector()->matches($node, $parentStack);
	}
	
}
