<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssAttributeFirstSelector class file.
 * 
 * This class is a simple implementation of the CssAttributeSelectorInterface.
 * 
 * @author Anastaszor
 */
class CssAttributeFirstSelector extends CssAbstractAttributeSelector
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAttributeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return '|=';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self
			&& parent::equals($object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		$attribute = $node->getAttribute($this->getName());
		if(null === $attribute)
		{
			return false;
		}
		if($this->isAny())
		{
			return true;
		}
		
		return $attribute->valueWordBeginsWith($this->getValue());
	}
	
}
