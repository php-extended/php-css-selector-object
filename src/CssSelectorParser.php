<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * CssSelectorParser class file.
 * 
 * This class is a simple implementatioon of the CssSelectorParserInterface
 * based on a char lexer.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<CssAbstractSelectorInterface>
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class CssSelectorParser extends AbstractParserLexer implements CssSelectorParserInterface
{
	
	public const L_CHR_ALPHANUM = 1; // alpha num and -
	public const L_CHR_WHITESPACE = 2; // sp
	public const L_CHR_DOT = 3; // .
	public const L_CHR_HASHTAG = 4; // #
	public const L_CHR_STAR = 5; // *
	public const L_CHR_COMMA = 6; // ,
	public const L_CHR_PARENT = 7; // >
	public const L_CHR_PLUS = 8; // +
	public const L_CHR_TILDE = 9; // ~
	public const L_CHR_OP_SQ_BRAQ = 10; // [
	public const L_CHR_CL_SQ_BRAQ = 11; // ]
	public const L_CHR_EQUAL = 12; // =
	public const L_CHR_DQUOTE = 13; // "
	public const L_CHR_VBAR = 14; // |
	public const L_CHR_BEGIN = 15; // ^
	public const L_CHR_END = 16; // $
	public const L_CHR_COLON = 17; // :
	public const L_CHR_OP_PARENTH = 18; // (
	public const L_CHR_CL_PARENTH = 19; // )
	public const L_CHR_ANTISLASH = 20; // \
	
	public const L_OP_DCOLON = 21; // ::
	public const L_OP_SUBSTRING = 22; // ~=
	public const L_OP_STARTING = 23; // |=
	public const L_OP_BEGINNING = 24; // ^=
	public const L_OP_ENDING = 25; // $=
	public const L_OP_CONTAINING = 26; // *=
	
	public const L_SEL_NAMESPACE = 27; // element|
	public const L_SEL_ELEMENT = 28; // element
	public const L_SEL_CLASS = 29; // .element
	public const L_SEL_ID = 30; // #element
	public const L_SEL_PSCLASS = 31; // :element
	
	public const L_PSFUNCTION_BEG = 32; // :func(
	public const L_FUNCTION_BEGIN = 33; // func(
	public const L_QUOTED_STR = 34; // "
	public const L_QUOTED_ESC = 35; // "str\
	public const L_QUOTED_COMPL = 36; // "string" (used in functions)
	public const L_CHR_ALNUM_ESC = 37; // str\
	
	/**
	 * Builds a new CssSelectorParser with its configuration.
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function __construct()
	{
		parent::__construct(CssAbstractSelectorInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::L_CHR_ALPHANUM);
		$this->_config->addMappings('-', self::L_CHR_ALPHANUM);
		$this->_config->addMappings('_', self::L_CHR_ALPHANUM);
		$this->_config->addMappings(LexerInterface::CLASS_SPACE, self::L_CHR_WHITESPACE);
		$this->_config->addMappings('.', self::L_CHR_DOT);
		$this->_config->addMappings('#', self::L_CHR_HASHTAG);
		$this->_config->addMappings('*', self::L_CHR_STAR);
		$this->_config->addMappings(',', self::L_CHR_COMMA);
		$this->_config->addMappings('>', self::L_CHR_PARENT);
		$this->_config->addMappings('+', self::L_CHR_PLUS);
		$this->_config->addMappings('~', self::L_CHR_TILDE);
		$this->_config->addMappings('[', self::L_CHR_OP_SQ_BRAQ);
		$this->_config->addMappings(']', self::L_CHR_CL_SQ_BRAQ);
		$this->_config->addMappings('=', self::L_CHR_EQUAL);
		$this->_config->addMappings('"', self::L_CHR_DQUOTE);
		$this->_config->addMappings('|', self::L_CHR_VBAR);
		$this->_config->addMappings('^', self::L_CHR_BEGIN);
		$this->_config->addMappings('$', self::L_CHR_END);
		$this->_config->addMappings(':', self::L_CHR_COLON);
		$this->_config->addMappings('(', self::L_CHR_OP_PARENTH);
		$this->_config->addMappings(')', self::L_CHR_CL_PARENTH);
		$this->_config->addMappings('\\', self::L_CHR_ANTISLASH);
		
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM, self::L_CHR_ALPHANUM);
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_WHITESPACE, self::L_CHR_WHITESPACE);
		$this->_config->addMerging(self::L_CHR_COLON, self::L_CHR_COLON, self::L_OP_DCOLON);
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_ANTISLASH, self::L_CHR_ALNUM_ESC);
		$this->_config->addMerging(self::L_CHR_ALNUM_ESC, LexerInterface::L_EVERYTHING_ELSE, self::L_CHR_ALPHANUM);
		
		// collapse " . " to "."
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_DOT, self::L_CHR_DOT);
		$this->_config->addMerging(self::L_CHR_DOT, self::L_CHR_WHITESPACE, self::L_CHR_DOT);
		
		// collapse " # " to "#"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_HASHTAG, self::L_CHR_HASHTAG);
		$this->_config->addMerging(self::L_CHR_HASHTAG, self::L_CHR_WHITESPACE, self::L_CHR_HASHTAG);
		
		// collapse " * " to "*"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_STAR, self::L_CHR_STAR);
		$this->_config->addMerging(self::L_CHR_STAR, self::L_CHR_WHITESPACE, self::L_CHR_STAR);
		
		// collapse " , " to ","
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_COMMA, self::L_CHR_COMMA);
		$this->_config->addMerging(self::L_CHR_COMMA, self::L_CHR_WHITESPACE, self::L_CHR_COMMA);
		
		// collapse " > " to ">"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_PARENT, self::L_CHR_PARENT);
		$this->_config->addMerging(self::L_CHR_PARENT, self::L_CHR_WHITESPACE, self::L_CHR_PARENT);
		
		// collapse " + " to "+"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_PLUS, self::L_CHR_PLUS);
		$this->_config->addMerging(self::L_CHR_PLUS, self::L_CHR_WHITESPACE, self::L_CHR_PLUS);
		
		// collapse " ~ " to "~"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_TILDE, self::L_CHR_TILDE);
		$this->_config->addMerging(self::L_CHR_TILDE, self::L_CHR_WHITESPACE, self::L_CHR_TILDE);
		
		// collapse " ~ = " to "~="
		$this->_config->addMerging(self::L_CHR_TILDE, self::L_CHR_EQUAL, self::L_OP_SUBSTRING);
		$this->_config->addMerging(self::L_OP_SUBSTRING, self::L_CHR_WHITESPACE, self::L_OP_SUBSTRING);
		
		// collapse " | = " to "|="
		$this->_config->addMerging(self::L_CHR_VBAR, self::L_CHR_EQUAL, self::L_OP_STARTING);
		$this->_config->addMerging(self::L_OP_STARTING, self::L_CHR_WHITESPACE, self::L_OP_STARTING);
		
		// collapse " ^ = " to "^="
		$this->_config->addMerging(self::L_CHR_BEGIN, self::L_CHR_EQUAL, self::L_OP_BEGINNING);
		$this->_config->addMerging(self::L_OP_BEGINNING, self::L_CHR_WHITESPACE, self::L_OP_BEGINNING);
		
		// collapse " $ = " to "$="
		$this->_config->addMerging(self::L_CHR_END, self::L_CHR_EQUAL, self::L_OP_ENDING);
		$this->_config->addMerging(self::L_OP_ENDING, self::L_CHR_WHITESPACE, self::L_OP_ENDING);
		
		// collapse " * = " to "*="
		$this->_config->addMerging(self::L_CHR_STAR, self::L_CHR_EQUAL, self::L_OP_CONTAINING);
		$this->_config->addMerging(self::L_OP_CONTAINING, self::L_CHR_WHITESPACE, self::L_OP_CONTAINING);
		
		// ns|el
		// commented because conflicts with [attr|=val]
// 		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_VBAR, self::L_SEL_NAMESPACE);
		$this->_config->addMerging(self::L_SEL_NAMESPACE, self::L_CHR_ALPHANUM, self::L_SEL_ELEMENT);
		$this->_config->addMerging(self::L_SEL_ELEMENT, self::L_CHR_ALPHANUM, self::L_SEL_ELEMENT);
		$this->_config->addMerging(self::L_CHR_STAR, self::L_CHR_VBAR, self::L_SEL_NAMESPACE);
		
		// .elem
		$this->_config->addMerging(self::L_CHR_DOT, self::L_CHR_ALPHANUM, self::L_SEL_CLASS);
		$this->_config->addMerging(self::L_SEL_CLASS, self::L_CHR_ALPHANUM, self::L_SEL_CLASS);
		
		// #elem
		$this->_config->addMerging(self::L_CHR_HASHTAG, self::L_CHR_ALPHANUM, self::L_SEL_ID);
		$this->_config->addMerging(self::L_SEL_ID, self::L_CHR_ALPHANUM, self::L_SEL_ID);
		
		// :elem
		$this->_config->addMerging(self::L_CHR_COLON, self::L_CHR_ALPHANUM, self::L_SEL_PSCLASS);
		$this->_config->addMerging(self::L_OP_DCOLON, self::L_CHR_ALPHANUM, self::L_SEL_PSCLASS);
		$this->_config->addMerging(self::L_SEL_PSCLASS, self::L_CHR_ALPHANUM, self::L_SEL_PSCLASS);
		
		// func( // :func(
		$this->_config->addMerging(self::L_SEL_PSCLASS, self::L_CHR_OP_PARENTH, self::L_PSFUNCTION_BEG);
		$this->_config->addMerging(self::L_CHR_ALPHANUM, self::L_CHR_OP_PARENTH, self::L_FUNCTION_BEGIN);
		
		// "str\"ing"
		$this->_config->addMerging(self::L_CHR_WHITESPACE, self::L_CHR_DQUOTE, self::L_CHR_DQUOTE);
		$this->_config->addMerging(self::L_CHR_DQUOTE, self::L_CHR_DQUOTE, self::L_QUOTED_COMPL);
		$this->_config->addMerging(self::L_CHR_DQUOTE, self::L_CHR_ANTISLASH, self::L_QUOTED_ESC);
		$this->_config->addMerging(self::L_CHR_DQUOTE, LexerInterface::L_EVERYTHING_ELSE, self::L_QUOTED_STR);
		$this->_config->addMerging(self::L_QUOTED_STR, self::L_CHR_DQUOTE, self::L_QUOTED_COMPL);
		$this->_config->addMerging(self::L_QUOTED_STR, self::L_CHR_ANTISLASH, self::L_QUOTED_ESC);
		$this->_config->addMerging(self::L_QUOTED_STR, LexerInterface::L_EVERYTHING_ELSE, self::L_QUOTED_STR);
		$this->_config->addMerging(self::L_QUOTED_ESC, LexerInterface::L_EVERYTHING_ELSE, self::L_QUOTED_STR);
	}
	
	/**
	 * Parses the data that comes from the given lexer.
	 * 
	 * @param LexerInterface $lexer
	 * @param boolean $rewind
	 * @return CssAbstractSelectorInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function parseLexer(LexerInterface $lexer, bool $rewind = true) : CssAbstractSelectorInterface
	{
		$cssStack = new CssSelectorStack();
		
		if($rewind)
		{
			$lexer->rewind();
		}
		
		while($lexer->valid())
		{
			$token = $lexer->current();
			
			switch($token->getCode())
			{
				case self::L_CHR_DOT:
				case self::L_CHR_HASHTAG:
				case self::L_CHR_EQUAL:
				case self::L_CHR_DQUOTE:
				case self::L_CHR_VBAR:
				case self::L_CHR_BEGIN:
				case self::L_CHR_END:
				case self::L_CHR_COLON:
				case self::L_CHR_OP_PARENTH:
				case self::L_CHR_CL_SQ_BRAQ:
				case self::L_CHR_ANTISLASH:
				case self::L_OP_BEGINNING:
				case self::L_OP_SUBSTRING:
				case self::L_OP_DCOLON:
				case self::L_OP_ENDING:
				case self::L_OP_STARTING:
				case self::L_OP_CONTAINING:
					// else ignore
					break;
					
				case self::L_CHR_CL_PARENTH:
					// we are in a function, escape it !
					if(!$rewind)
					{
						break 2;
					}
					// else ignore
					break;
					
				case self::L_CHR_OP_SQ_BRAQ:
					$lexer->next();
					$currentNode = $this->parseAttribute($lexer);
					if(null !== $currentNode)
					{
						$cssStack->addNode($currentNode);
					}
					break;
					
				case self::L_CHR_STAR:
					if(\mb_strlen(\trim($token->getData())) !== \mb_strlen(\rtrim($token->getData())))
					{
						$cssStack->addOperator(CssSelectorStack::OP_DESCENDANT);
					}
					// No break, cascades
					
				case self::L_CHR_ALPHANUM:
				case self::L_SEL_ELEMENT:
				case self::L_SEL_NAMESPACE:
					if(false !== \mb_strpos($token->getData(), '|'))
					{
						$parts = \explode('|', $token->getData(), 2);
						if('' === (\trim($parts[0])))
						{
							$parts[0] = '*';
						}
						if('' === (\trim($parts[1] ?? '')))
						{
							$parts[1] = '*';
						}
						$currentNode = new CssElementSelector($parts[1] ?? '*', $parts[0]);
						$cssStack->addNode($currentNode);
						break;
					}
					$currentNode = new CssElementSelector($token->getData());
					$cssStack->addNode($currentNode);
					break;
				
				case self::L_CHR_WHITESPACE:
					$cssStack->addOperator(CssSelectorStack::OP_DESCENDANT);
					break;
				
				case self::L_CHR_PARENT:
					$cssStack->addOperator(CssSelectorStack::OP_PARENT);
					break;
				
				case self::L_CHR_PLUS:
					$cssStack->addOperator(CssSelectorStack::OP_NEXT_SIBLING);
					break;
				
				case self::L_CHR_TILDE:
					$cssStack->addOperator(CssSelectorStack::OP_PREV_SIBLING);
					break;
				
				case self::L_CHR_COMMA:
					$cssStack->addOperator(CssSelectorStack::OP_OR);
					break;
					
				case self::L_SEL_CLASS:
					$tdata = $token->getData();
					if(\mb_strlen(\ltrim($tdata)) !== \mb_strlen($tdata))
					{
						// we captured a space
						$cssStack->addOperator(CssSelectorStack::OP_DESCENDANT);
						$tdata = \ltrim($tdata);
					}
					
					$currentNode = new CssAttributeWordSelector('class', \ltrim($tdata, '.'));
					$cssStack->addNode($currentNode);
					break;
					
				case self::L_SEL_ID:
					$tdata = $token->getData();
					if(\mb_strlen(\ltrim($tdata)) !== \mb_strlen($tdata))
					{
						// we captured a space
						$cssStack->addOperator(CssSelectorStack::OP_DESCENDANT);
						$tdata = \ltrim($tdata);
					}
					
					$currentNode = new CssAttributeEqualsSelector('id', \ltrim($tdata, '#'));
					$cssStack->addNode($currentNode);
					break;
					
				case self::L_SEL_PSCLASS:
					$tdata = $token->getData();
					if(\mb_strlen(\ltrim($tdata)) !== \mb_strlen($tdata))
					{
						// we captured a space
						$cssStack->addOperator(CssSelectorStack::OP_DESCENDANT);
						$tdata = \ltrim($tdata);
					}
					
					$currentNode = $this->getState(\ltrim($tdata, ':'));
					if(null !== $currentNode)
					{
						$cssStack->addNode($currentNode);
					}
					break;
					
				case self::L_PSFUNCTION_BEG:
				case self::L_FUNCTION_BEGIN:
					$lexer->next();
					$currentNode = $this->parseLexer($lexer, false);
					$tdata = \trim(\trim($token->getData(), ':'), '(');
					
					switch($tdata)
					{
						case 'nth-child':
						case 'nth-last-child':
						case 'nth-last-of-type':
						case 'nth-of-type':
							while($currentNode instanceof CssCompositeSelectorInterface)
							{
								$currentNode = $currentNode->getBeforeSelector();
							}
							$nth = (int) $currentNode->getName();
							
							switch($tdata)
							{
								case 'nth-child':
									$currentNode = new CssStateNthChildSelector($nth);
									break;
									
								case 'nth-last-child':
									$currentNode = new CssStateNthLastChildSelector($nth);
									break;
									
								case 'nth-last-of-type':
									$currentNode = new CssStateNthLastOfTypeSelector($nth);
									break;
									
								case 'nth-of-type':
									$currentNode = new CssStateNthOfTypeSelector($nth);
									break;
							}
							$cssStack->addNode($currentNode);
							break;
						
						case 'lang':
							while($currentNode instanceof CssCompositeSelectorInterface)
							{
								$currentNode = $currentNode->getBeforeSelector();
							}
							$currentNode = new CssAttributeEqualsSelector('lang', $currentNode->getName());
							$cssStack->addNode($currentNode);
							break;
						
						case 'not':
							$cssStack->addOperator(CssSelectorStack::OP_NOT);
							$cssStack->addNode($currentNode);
							break;
						default:
							// ignore everything parsed in the ( ... )
							break;
					}
					break;
					
				case self::L_QUOTED_STR:
				case self::L_QUOTED_ESC:
				case self::L_QUOTED_COMPL:
					$str = \ltrim($token->getData());
					if('"' === $str[(int) \mb_strlen($str) - 1])
					{
						$str = (string) \mb_substr($str, 0, -1);
					}
					if('"' === $str[0])
					{
						$str = (string) \mb_substr($str, 1);
					}
					$str = \strtr($str, ['\\"' => '"', '\\\\' => '\\']);
					$currentNode = new CssElementSelector($str);
					$cssStack->addNode($currentNode);
					break;
					
				default:
					// ignore
					break;
			}
			
			$lexer->next();
		}
		
		return $cssStack->getCompact();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssSelectorParserInterface::parseString()
	 */
	public function parse(?string $data) : CssAbstractSelectorInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * Parses lexer data in attribute context [attr=value]
	 * (after finding "[" ).
	 * 
	 * @param LexerInterface $lexer
	 * @return ?CssAbstractSelectorInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	protected function parseAttribute(LexerInterface $lexer) : ?CssAbstractSelectorInterface
	{
		$attrName = null;
		$attrOp = null;
		$attrValue = '';
		
		// no rewind !
		while($lexer->valid())
		{
			$token = $lexer->current();
			
			switch($token->getCode())
			{
				case self::L_CHR_ALPHANUM:
					if(null === $attrName)
					{
						$attrName = $token->getData();
						break;
					}
					$attrValue .= $token->getData();
					break;
					
				case self::L_CHR_EQUAL:
				case self::L_OP_DCOLON:
				case self::L_OP_SUBSTRING:
				case self::L_OP_STARTING:
				case self::L_OP_BEGINNING:
				case self::L_OP_ENDING:
				case self::L_OP_CONTAINING:
					if(null === $attrOp)
					{
						$attrOp = $token->getCode();
						break;
					}
					$attrValue .= $token->getData();
					break;
					
				case self::L_SEL_NAMESPACE:
				case self::L_SEL_ELEMENT:
					if(null === $attrName)
					{
						$attrName = $token->getData();
						break;
					}
					$attrValue .= $token->getData();
					break;
				
				case self::L_QUOTED_STR:
				case self::L_QUOTED_ESC:
				case self::L_QUOTED_COMPL:
					$val = \ltrim($token->getData());
					if('"' === $val[(int) \mb_strlen($val) - 1])
					{
						$val = (string) \mb_substr($val, 0, -1);
					}
					if('"' === $val[0])
					{
						$val = (string) \mb_substr($val, 1);
					}
					$val = \strtr($val, ['\\"' => '"', '\\\\' => '\\']);
					$attrValue = $val;
					break;
					
				case self::L_CHR_CL_SQ_BRAQ:
					break 2;
					
				case self::L_CHR_WHITESPACE:
				case self::L_CHR_DOT:
				case self::L_CHR_HASHTAG:
				case self::L_CHR_STAR:
				case self::L_CHR_COMMA:
				case self::L_CHR_PARENT:
				case self::L_CHR_PLUS:
				case self::L_CHR_TILDE:
				case self::L_CHR_OP_SQ_BRAQ:
				case self::L_CHR_DQUOTE:
				case self::L_CHR_VBAR:
				case self::L_CHR_BEGIN:
				case self::L_CHR_END:
				case self::L_CHR_COLON:
				case self::L_CHR_OP_PARENTH:
				case self::L_CHR_CL_PARENTH:
				case self::L_CHR_ANTISLASH:
				case self::L_SEL_CLASS:
				case self::L_SEL_ID:
				case self::L_SEL_PSCLASS:
				case self::L_PSFUNCTION_BEG:
				case self::L_FUNCTION_BEGIN:
				default:
					$attrValue .= $token->getData();
			}
			
			$lexer->next();
		}
		
		if(null === $attrName)
		{
			return new CssAttributeExistsSelector('*');
		}
		
		if(empty($attrValue))
		{
			$attrValue = '*';
		}
		
		switch($attrOp)
		{
			case self::L_OP_CONTAINING:
				return new CssAttributeContainsSelector($attrName, $attrValue);
			
			case self::L_OP_BEGINNING:
				return new CssAttributeBeginsSelector($attrName, $attrValue);
			
			case self::L_OP_ENDING:
				return new CssAttributeEndsSelector($attrName, $attrValue);
			
			case self::L_OP_STARTING:
				return new CssAttributeFirstSelector($attrName, $attrValue);
			
			case self::L_OP_SUBSTRING:
				return new CssAttributeWordSelector($attrName, $attrValue);
			
			case self::L_CHR_EQUAL:
			default:
				if('*' === \trim($attrValue) || 0 === \mb_strlen(\trim($attrValue)))
				{
					return new CssAttributeExistsSelector($attrName);
				}
		}
		
		return new CssAttributeEqualsSelector($attrName, $attrValue);
	}
	
	/**
	 * Gets the state of the attribute with the state name. null if not found.
	 * 
	 * @param string $stateName
	 * @return ?CssAbstractSelectorInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function getState(string $stateName) : ?CssAbstractSelectorInterface
	{
		switch(\ltrim((string) \mb_strtolower($stateName), ':'))
		{
			case 'active':
				return new CssStateActiveSelector();
			
			case 'after':
				return new CssStateAfterSelector();
			
			case 'before':
				return new CssStateBeforeSelector();
			
			case 'checked':
				return new CssStateCheckedSelector();
			
			case 'default':
				return new CssStateDefaultSelector();
			
			case 'disabled':
				return new CssStateDisabledSelector();
			
			case 'empty':
				return new CssStateEmptySelector();
			
			case 'enabled':
				return new CssStateEnabledSelector();
			
			case 'first-child':
				return new CssStateFirstChildSelector();
			
			case 'first-letter':
				return new CssStateFirstLetterSelector();
			
			case 'first-line':
				return new CssStateFirstLineSelector();
			
			case 'first-of-type':
				return new CssStateFirstOfTypeSelector();
			
			case 'focus':
				return new CssStateFocusSelector();
			
			case 'hover':
				return new CssStateHoverSelector();
			
			case 'in-range':
				return new CssStateInRangeSelector();
			
			case 'indeterminate':
				return new CssStateIndeterminateSelector();
			
			case 'invalid':
				return new CssStateInvalidSelector();
			
			case 'lang':
				return new CssAttributeExistsSelector('lang');
			
			case 'last-child':
				return new CssStateLastChildSelector();
			
			case 'last-of-type':
				return new CssStateLastOfTypeSelector();
			
			case 'link':
				return new CssStateLinkSelector();
			
			case 'nth-child':
				return new CssStateNthChildSelector(1);
			
			case 'nth-last-child':
				return new CssStateNthLastChildSelector(1);
			
			case 'nth-last-of-type':
				return new CssStateNthLastOfTypeSelector(1);
			
			case 'nth-of-type':
				return new CssStateNthOfTypeSelector(1);
			
			case 'only-of-type':
				return new CssStateOnlyOfTypeSelector();
			
			case 'only-child':
				return new CssStateOnlyChildSelector();
			
			case 'optional':
				return new CssStateOptionalSelector();
			
			case 'out-of-range':
				return new CssStateOutOfRangeSelector();
			
			case 'placeholder':
				return new CssStatePlaceholderSelector();
			
			case 'read-only':
				return new CssStateReadOnlySelector();
			
			case 'read-write':
				return new CssStateReadWriteSelector();
			
			case 'required':
				return new CssStateRequiredSelector();
			
			case 'root':
				return new CssStateRootSelector();
			
			case 'selection':
				return new CssStateSelectionSelector();
			
			case 'target':
				return new CssStateTargetSelector();
			
			case 'valid':
				return new CssStateValidSelector();
			
			case 'visited':
				return new CssStateVisitedSelector();
		}
		
		return null;
	}
	
}
