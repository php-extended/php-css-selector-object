<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssAttributeExistsSelector class file.
 * 
 * This class is a simple implementation of the CssAttributeSelectorInterface.
 * 
 * @author Anastaszor
 */
class CssAttributeExistsSelector extends CssAbstractAttributeSelector
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::__toString()
	 */
	public function __toString() : string
	{
		return '['.$this->getName().']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAttributeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAttributeSelectorInterface::getValue()
	 */
	public function getValue() : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self
			&& parent::equals($object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return $node->hasAttribute($this->getName());
	}
	
}
