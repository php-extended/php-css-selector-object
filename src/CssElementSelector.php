<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssElementSelector class file.
 * 
 * This class is a simple implementation of the CssAbstractSelectorInterface.
 * 
 * @author Anastaszor
 */
class CssElementSelector extends CssAbstractSelector implements CssElementSelectorInterface
{
	
	/**
	 * The namespace of the element.
	 * 
	 * @var string
	 */
	protected string $_namespace = '*';
	
	/**
	 * Builds a new CssElementSelector with the given name and namespace.
	 * 
	 * @param string $name
	 * @param string $namespace
	 */
	public function __construct(string $name, string $namespace = '*')
	{
		parent::__construct($name);
		$this->_namespace = $namespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::__toString()
	 */
	public function __toString() : string
	{
		$str = ('*' === $this->_namespace ? '' : $this->_namespace.'|');
		
		return $str.parent::__toString();
	}
	
	/**
	 * Gets the namespace of the element selector.
	 * 
	 * @return string
	 */
	public function getNamespace() : string
	{
		return $this->_namespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_namespace) && parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::isAny()
	 */
	public function isAny() : bool
	{
		return '*' === $this->_namespace && parent::isAny();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return parent::equals($object)
			&& $object instanceof CssElementSelectorInterface
			&& $this->getNamespace() === $object->getNamespace();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::beVisitedBy()
	 */
	public function beVisitedBy(CssSelectorVisitorInterface $visitor)
	{
		return $visitor->visitElementSelector($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return $this->isAny() || $node->getName() === $this->getName();
	}
	
}
