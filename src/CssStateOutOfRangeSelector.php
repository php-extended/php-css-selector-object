<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateOutOfRangeSelector class file.
 * 
 * This class represents the :out-of-range pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateOutOfRangeSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateOutOfRangeSelector.
	 */
	public function __construct()
	{
		parent::__construct('out-of-range');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if(!$node->hasAttribute('value'))
		{
			return false;
		}
		
		$value = 0;
		$attribute = $node->getAttribute('value');
		if(null !== $attribute)
		{
			$value = (float) $attribute->getValue();
		}
		
		$attribute = $node->getAttribute('min');
		if(null !== $attribute)
		{
			$minval = (float) $attribute->getValue();
			if($value < $minval)
			{
				return true;
			}
		}
		
		$attribute = $node->getAttribute('max');
		if(null !== $attribute)
		{
			$maxval = (float) $attribute->getValue();
			if($value > $maxval)
			{
				return true;
			}
		}
		
		return false;
	}
	
}
