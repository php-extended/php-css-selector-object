<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateInvalidSelector class file.
 * 
 * This class represents the :invalid pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateInvalidSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateInvalidSelector.
	 */
	public function __construct()
	{
		parent::__construct('invalid');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if('input' === $node->getName())
		{
			$attribute = $node->getAttribute('type');
			if(null !== $attribute)
			{
				switch($attribute->getValue())
				{
					case 'button':         // Defines a clickable button (mostly used with a JavaScript to activate a script)
					case 'checkbox':       // Defines a checkbox
					case 'color':          // Defines a color picker
					case 'date':           // Defines a date control (year, month, day (no time))
					case 'datetime-local': // Defines a date and time control (year, month, day, time (no timezone)
					case 'email':          // Defines a field for an e-mail address
					case 'file':           // Defines a file-select field and a "Browse" button (for file uploads)
					case 'hidden':         // Defines a hidden input field
					case 'image':          // Defines an image as the submit button
					case 'month':          // Defines a month and year control (no timezone)
					case 'number':         // Defines a field for entering a number
					case 'password':       // Defines a password field
					case 'radio':          // Defines a radio button
					case 'range':          // Defines a range control (like a slider control)
					case 'reset':          // Defines a reset button
					case 'search':         // Defines a text field for entering a search string
					case 'submit':         // Defines a submit button
					case 'tel':            // Defines a field for entering a telephone number
					case 'text':           // Default. Defines a single-line text field
					case 'time':           // Defines a control for entering a time (no timezone)
					case 'url':            // Defines a field for entering a URL
					case 'week':           // Defines a week and year control (no timezone)
						return true;
				}
			}
		}
		
		return false;
	}
	
}
