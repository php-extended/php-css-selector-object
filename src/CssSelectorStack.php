<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use SplStack;
use Stringable;

/**
 * CssSelectorStack class file.
 * 
 * This class represents the management of the shunting yard algorithm with the
 * twists of the css part, which are that there may exists two consecutive
 * nodes and an implicit operator, or two consecutive operators with an implicit
 * node.
 * 
 * @see https://en.wikipedia.org/wiki/Shunting-yard_algorithm
 * @see http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
 * @see https://stackoverflow.com/questions/28256/equation-expression-parser-with-precedence
 * 
 * @author Anastaszor
 * @psalm-suppress TooManyTemplateParams
 */
class CssSelectorStack implements Stringable
{
	
	// all operators are left associative (which facilitates parsing)
	// operator precedence is the following :
	// "," has the least precedence
	// " " (descendant), ">" (parent), "~" (next sibling), and "+" (prev sibling) have the same precedence
	// "" (and) has a greater precedence
	// ":not()" has the greater precedence
	public const OP_NOT = 1;
	public const OP_AND = 2;
	public const OP_DESCENDANT = 3;
	public const OP_PARENT = 4;
	public const OP_NEXT_SIBLING = 5;
	public const OP_PREV_SIBLING = 6;
	public const OP_OR = 7;
	
	/**
	 * The stack of operators.
	 * 
	 * @var SplStack<int>
	 */
	protected SplStack $_opStack;
	
	/**
	 * The stack of nodes.
	 * 
	 * @var SplStack<CssAbstractSelectorInterface>
	 */
	protected SplStack $_nodeStack;
	
	/**
	 * Builds a new CssSelectorStack with no args.
	 */
	public function __construct()
	{
		/** @psalm-suppress MixedPropertyTypeCoercion */
		$this->_opStack = new SplStack();
		/** @psalm-suppress MixedPropertyTypeCoercion */
		$this->_nodeStack = new SplStack();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return self::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a node to this selector stack.
	 * 
	 * @param CssAbstractSelectorInterface $selector
	 */
	public function addNode(CssAbstractSelectorInterface $selector) : void
	{
		// while count(node_stack) at least == 1 + count(op_stack)
		// since we will add an element to the node stack
		// in order to maintain the count of the op stack at most one 
		// behind the node stack
		// we have to add implicit op elements (AND)
		while($this->_nodeStack->count() > $this->_opStack->count())
		{
			$this->addOperator(self::OP_AND);
		}
		$this->_nodeStack->push($selector);
	}
	
	/**
	 * Adds an operator to this selector stack.
	 * 
	 * @param int $operator
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function addOperator(int $operator) : void
	{
		// we have to adapt the shunting yard algorithm :
		// because in css there are implicit (and) nodes that are given every
		// time a non operator node is added, we have to add additional (all) 
		// selector when there are not enough (may be the case if all stacks
		// are empty and we begin by adding a new op like, not, which would
		// be valid css)
		while($this->_opStack->count() >= $this->_nodeStack->count())
		{
			$this->addNode(new CssElementSelector('*'));
		}
		
		// shunting yard junction
		// we have to examin the priority of ops, and while they are all left
		// associative, only their relative priority determines whether we
		// should compact the existing nodes and ops.
		// priority equals still trigger the compacting, because the css 
		// queries the node that is rightmost of the string, so it should be
		// on the top of the selector tree, with previous string as before node
		while(0 < $this->_opStack->count() && $this->getPriority($this->_opStack->top()) <= $this->getPriority($operator))
		{
			$rightNode = $this->_nodeStack->pop();
			$leftNode = $this->_nodeStack->pop();
			$pop = $this->_opStack->pop();
			
			switch($pop)
			{
				case self::OP_NOT:
					$newNode = new CssCompositeNotSelector($leftNode, $rightNode);
					break;
				
				case self::OP_DESCENDANT:
					$newNode = new CssCompositeDescendantSelector($leftNode, $rightNode);
					break;
				
				case self::OP_PARENT:
					$newNode = new CssCompositeInsideSelector($leftNode, $rightNode);
					break;
				
				case self::OP_NEXT_SIBLING:
					$newNode = new CssCompositeAfterSelector($leftNode, $rightNode);
					break;
				
				case self::OP_PREV_SIBLING:
					$newNode = new CssCompositeBeforeSelector($leftNode, $rightNode);
					break;
				
				case self::OP_OR:
					$newNode = new CssCompositeOrSelector($leftNode, $rightNode);
					break;
				
				case self::OP_AND:
				default:
					$newNode = new CssCompositeAndSelector($leftNode, $rightNode);
					break;
			}
			$this->_nodeStack->push($newNode);
		}
		
		$this->_opStack->push($operator);
	}
	
	/**
	 * Flattens the node and op stack and gives back the resulting node tree.
	 * 
	 * @return CssAbstractSelectorInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function getCompact() : CssAbstractSelectorInterface
	{
		while(1 < $this->_nodeStack->count() && 0 < $this->_opStack->count())
		{
			$rightNode = $this->_nodeStack->pop();
			$leftNode = $this->_nodeStack->pop();
			$pop = $this->_opStack->pop();
			
			switch($pop)
			{
				case self::OP_NOT:
					$newNode = new CssCompositeNotSelector($leftNode, $rightNode);
					break;
				
				case self::OP_DESCENDANT:
					$newNode = new CssCompositeDescendantSelector($leftNode, $rightNode);
					break;
				
				case self::OP_PARENT:
					$newNode = new CssCompositeInsideSelector($leftNode, $rightNode);
					break;
				
				case self::OP_NEXT_SIBLING:
					$newNode = new CssCompositeAfterSelector($leftNode, $rightNode);
					break;
				
				case self::OP_PREV_SIBLING:
					$newNode = new CssCompositeBeforeSelector($leftNode, $rightNode);
					break;
				
				case self::OP_OR:
					$newNode = new CssCompositeOrSelector($leftNode, $rightNode);
					break;
				
				case self::OP_AND:
				default:
					$newNode = new CssCompositeAndSelector($leftNode, $rightNode);
					break;
			}
			$this->_nodeStack->push($newNode);
		}
		
		// handles errors, should not be used
		while(1 < $this->_nodeStack->count())
		{
			$rightNode = $this->_nodeStack->pop();
			$leftNode = $this->_nodeStack->pop();
			$this->_nodeStack->push(new CssCompositeAndSelector($leftNode, $rightNode));
		}
		
		/** @psalm-suppress MixedPropertyTypeCoercion */
		$this->_opStack = new SplStack(); // reset for(hypothetical) future use
		
		if($this->_nodeStack->isEmpty())
		{
			return new CssElementSelector('*');
		}
		
		return $this->_nodeStack->pop();
	}
	
	/**
	 * Gets the priority of the given operator. Ranges from 1 to 3, where 1
	 * is assumed to be the least priorisable.
	 * 
	 * @param integer $operator
	 * @return integer
	 */
	protected function getPriority(int $operator) : int
	{
		switch($operator)
		{
			case self::OP_NOT:
				return 1;
			
			case self::OP_AND:
				return 2;
			
			case self::OP_OR:
				return 4;
			
			default:
				return 3;
		}
	}
	
}
