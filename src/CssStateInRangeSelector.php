<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateInRangeSelector class file.
 * 
 * This class represents the :in-range pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateInRangeSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateInRangeSelector.
	 */
	public function __construct()
	{
		parent::__construct('in-range');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		$attribute = $node->getAttribute('value');
		if(null === $attribute)
		{
			return false;
		}
		$value = (float) $attribute->getValue();
		
		$attribute = $node->getAttribute('min');
		if(null !== $attribute)
		{
			$minval = (float) $attribute->getValue();
			if($value < $minval)
			{
				return false;
			}
		}
		
		$attribute = $node->getAttribute('max');
		if(null !== $attribute)
		{
			$maxval = (float) $attribute->getValue();
			if($value > $maxval)
			{
				return false;
			}
		}
		
		return true;
	}
	
}
