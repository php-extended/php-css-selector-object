<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateTargetSelector class file.
 * 
 * This class represents the :target pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateTargetSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateTargetSelector.
	 */
	public function __construct()
	{
		parent::__construct('target');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return false;
	}
	
}
