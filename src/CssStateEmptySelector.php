<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use SplStack;

/**
 * CssStateEmptySelector class file.
 * 
 * This class represents the :empty pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateEmptySelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateEmptySelector.
	 */
	public function __construct()
	{
		parent::__construct('empty');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if($node instanceof HtmlCollectionNodeInterface)
		{
			return 0 === $node->countChildren();
		}
		
		return false;
	}
	
}
