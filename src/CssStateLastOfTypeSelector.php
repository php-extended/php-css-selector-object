<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateLastOfTypeSelector class file.
 * 
 * This class represents the :last-of-type pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateLastOfTypeSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateLastOfTypeSelector.
	 */
	public function __construct()
	{
		parent::__construct('last-of-type');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if(null === $parentStack || $parentStack->isEmpty())
		{
			return false;
		}
		
		/** @var \PhpExtended\Html\HtmlCollectionNodeInterface $parent */
		$parent = $parentStack->top();
		
		return $parent->getLastChild($this->getName()) === $node;
	}
	
}
