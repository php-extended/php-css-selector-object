<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssSelectorTreeDumper class file.
 * 
 * This class represents a walker of a css tree that echoes its contents.
 * 
 * @author Anastaszor
 */
class CssSelectorTreeDumper implements CssSelectorVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssSelectorVisitorInterface::visitAttributeSelector()
	 */
	public function visitAttributeSelector(CssAttributeSelectorInterface $selector)
	{
		return '['.$selector->getName().\rtrim(' '.$selector->getOperator().' '.$selector->getValue()).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssSelectorVisitorInterface::visitCompositeSelector()
	 */
	public function visitCompositeSelector(CssCompositeSelectorInterface $selector)
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
		$ret = ((string) $selector->getBeforeSelector()->beVisitedBy($this)).' ';
		$ret .= $selector->getOperator().' ';
		$ret .= ('NOT' === $selector->getName() ? '(' : '');
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
		$ret .= ((string) $selector->getAfterSelector()->beVisitedBy($this));
		
		return $ret.('NOT' === $selector->getName() ? ')' : '');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssSelectorVisitorInterface::visitElementSelector()
	 */
	public function visitElementSelector(CssElementSelectorInterface $selector)
	{
		return ('*' === $selector->getNamespace() ? '' : $selector->getNamespace().'|')
			  .$selector->getName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssSelectorVisitorInterface::visitStateSelector()
	 */
	public function visitStateSelector(CssStateSelectorInterface $selector)
	{
		return ':'.$selector->getName().(0 < $selector->getQuantity() ? '('.((string) $selector->getQuantity()).')' : '');
	}
	
}
