<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateActiveSelector class file.
 * 
 * This class represents the :active pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateActiveSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateActiveSelector.
	 */
	public function __construct()
	{
		parent::__construct('active');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return false;
	}
	
}
