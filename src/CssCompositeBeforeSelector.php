<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssCompositeBeforeSelector class file.
 * 
 * This class represents a composite selector that matches a node that is 
 * directly after another node.
 * 
 * @author Anastaszor
 */
class CssCompositeBeforeSelector extends CssAbstractCompositeSelector
{
	
	/**
	 * Builds a new CssCompositeBeforeSelector with both selectors.
	 * 
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct('SBEF', $before, $after);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return '~';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if(null === $parentStack || $parentStack->isEmpty())
		{
			return false;
		}
		
		if(!$this->getAfterSelector()->matches($node, $parentStack))
		{
			return false;
		}
		
		/** @var \PhpExtended\Html\HtmlCollectionNodeInterface $parent */
		$parent = $parentStack->top();
		
		$lvl = -2;
		
		foreach($parent as $k => $element)
		{
			if($element === $node)
			{
				$lvl = (int) $k;
				continue;
			}
			if($lvl + 1 === (int) $k)
			{
				$pstack = clone $parentStack;
				$pstack->pop();
				
				return $this->getBeforeSelector()->matches($element, $pstack);
			}
		}
		
		return false;
	}
	
}
