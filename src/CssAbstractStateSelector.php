<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssAbstractStateSelector class file.
 * 
 * This class is a simple implementation of the CssStateSelectorInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.NumberOfChildren")
 */
abstract class CssAbstractStateSelector extends CssAbstractSelector implements CssStateSelectorInterface
{
	
	/**
	 * The quantity of the selector.
	 * 
	 * @var integer
	 */
	protected int $_quantity = 0;
	
	/**
	 * Builds a new CssAbstractStateSelector with the given name and quantity.
	 * 
	 * @param string $name
	 * @param int $quantity
	 */
	public function __construct(string $name, int $quantity = 0)
	{
		parent::__construct($name);
		$this->_quantity = $quantity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::__toString()
	 */
	public function __toString() : string
	{
		$str = (0 < $this->_quantity ? '('.((string) $this->getQuantity()).')' : '');
		
		return ':'.parent::__toString().$str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractStateSelector::getQuantity()
	 */
	public function getQuantity() : int
	{
		return $this->_quantity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::isAny()
	 */
	public function isAny() : bool
	{
		return 0 === $this->_quantity && parent::isAny();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return parent::equals($object)
			&& $object instanceof self
			&& $this->getQuantity() === $object->getQuantity();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::beVisitedBy()
	 */
	public function beVisitedBy(CssSelectorVisitorInterface $visitor)
	{
		return $visitor->visitStateSelector($this);
	}
	
}
