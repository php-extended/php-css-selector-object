<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssCompositeOrSelector class file.
 * 
 * This class represents a composite selector that matches a node if it matches
 * one of the node before or the node after.
 * 
 * @author Anastaszor
 */
class CssCompositeOrSelector extends CssAbstractCompositeSelector
{
	
	/**
	 * Builds a new CssCompositeOrSelector with both selectors.
	 * 
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct('OR', $before, $after);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return ',';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		return $this->getBeforeSelector()->matches($node, $parentStack)
			|| $this->getAfterSelector()->matches($node, $parentStack);
	}
	
}
