<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use ArrayIterator;
use Iterator;
use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use SplStack;

/**
 * CssAbstractSelector class file.
 * 
 * This class is a simple implementation of the CssAbstractSelectorInterface.
 * 
 * @author Anastaszor
 */
abstract class CssAbstractSelector implements CssAbstractSelectorInterface
{
	
	/**
	 * The name of the selector.
	 * 
	 * @var string
	 */
	protected string $_name = '*';
	
	/**
	 * Builds a new CssAbstractSelector with the given name.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		if('*' === $name)
		{
			$this->_name = $name;

			return;
		}
		
		$this->_name = (string) \preg_replace('#[^\\w_-]#', '', $name); // sanitize
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::getName()
	 */
	public function getName() : string
	{
		return (string) $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::isAny()
	 */
	public function isAny() : bool
	{
		return '*' === $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof CssAbstractSelectorInterface
			&& $this->getName() === $object->getName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::findNode()
	 */
	public function findNode(HtmlAbstractNodeInterface $tree, int $position = 0, ?SplStack $parentStack = null) : ?HtmlAbstractNodeInterface
	{
		if(0 <= $position)
		{
			foreach($this->findAllNodes($tree, $parentStack) as $curpos => $child)
			{
				if($position === ((int) $curpos))
				{
					return $child;
				}
			}
		}
		
		if(0 > $position)
		{
			$nodes = [];
			
			foreach($this->findAllNodes($tree, $parentStack) as $child)
			{
				$nodes[] = $child;
			}
			
			$count = \count($nodes);
			$npos = $count + $position;
			// effectively (count - pos) with pos negative
			if((0 <= $npos) && isset($nodes[$npos]))
			{
				return $nodes[$npos];
			}
		}
		
		// TODO optimize to find the nth node without the whole matching list
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::findAllNodes()
	 */
	public function findAllNodes(HtmlAbstractNodeInterface $tree, ?SplStack $parentStack = null) : Iterator
	{
		$matches = [];
		
		if($this->matches($tree, $parentStack))
		{
			$matches[] = $tree;
		}
		
		if($tree instanceof HtmlCollectionNodeInterface)
		{
			if(null === $parentStack)
			{
				/** @var SplStack<HtmlCollectionNodeInterface> $parentStack */
				$parentStack = new SplStack();
			}
			
			foreach($tree as $element)
			{
				$parentStack->unshift($tree);
				
				foreach($this->findAllNodes($element, $parentStack) as $match)
				{
					$matches[] = $match;
				}
				$parentStack->shift();
			}
		}
		
		return new ArrayIterator($matches);
	}
	
}
