<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssCompositeInsideSelector class file.
 * 
 * This class represents a composite selector that matches a node that is
 * directly inside another node.
 * 
 * @author Anastaszor
 */
class CssCompositeInsideSelector extends CssAbstractCompositeSelector
{
	
	/**
	 * Builds a new CssCompositeInsideSelector with both selectors.
	 * 
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct('INSD', $before, $after);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return '>';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if(null === $parentStack || $parentStack->isEmpty())
		{
			return false;
		}
		
		if(!$this->getAfterSelector()->matches($node, $parentStack))
		{
			return false;
		}
		
		$pstack = clone $parentStack;
		$parent = $pstack->pop();
		
		return $this->getBeforeSelector()->matches($parent, $pstack);
	}
	
}
