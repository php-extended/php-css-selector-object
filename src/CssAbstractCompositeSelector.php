<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssAbstractCompositeSelector class file.
 * 
 * This class represents an abstract composite selector.
 * 
 * @author Anastaszor
 */
abstract class CssAbstractCompositeSelector extends CssAbstractSelector implements CssCompositeSelectorInterface
{
	
	/**
	 * The node before.
	 * 
	 * @var CssAbstractSelectorInterface
	 */
	protected CssAbstractSelectorInterface $_before;
	
	/**
	 * The node after.
	 * 
	 * @var CssAbstractSelectorInterface
	 */
	protected CssAbstractSelectorInterface $_after;
	
	/**
	 * Builds a CssAbstractCompositeSelector with the given name and other
	 * nodes.
	 * 
	 * @param string $name
	 * @param CssAbstractSelectorInterface $before
	 * @param CssAbstractSelectorInterface $after
	 */
	public function __construct(string $name, CssAbstractSelectorInterface $before, CssAbstractSelectorInterface $after)
	{
		parent::__construct($name);
		$this->_before = $before;
		$this->_after = $after;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::__toString()
	 */
	public function __toString() : string
	{
		return $this->_before->__toString().$this->getOperator().$this->_after->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getBeforeSelector()
	 */
	public function getBeforeSelector() : CssAbstractSelectorInterface
	{
		return $this->_before;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssCompositeSelectorInterface::getAfterSelector()
	 */
	public function getAfterSelector() : CssAbstractSelectorInterface
	{
		return $this->_after;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return parent::equals($object)
			&& $object instanceof CssCompositeSelectorInterface
			&& $this->getBeforeSelector()->equals($object->getBeforeSelector())
			&& $this->getAfterSelector()->equals($object->getAfterSelector());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::beVisitedBy()
	 */
	public function beVisitedBy(CssSelectorVisitorInterface $visitor)
	{
		return $visitor->visitCompositeSelector($this);
	}
	
}
