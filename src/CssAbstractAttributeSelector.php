<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssAbstractAttributeSelector class file.
 * 
 * This class is a simple implementation of the CssAttributeSelectorInterface.
 * 
 * @author Anastaszor
 */
abstract class CssAbstractAttributeSelector extends CssAbstractSelector implements CssAttributeSelectorInterface
{
	
	/**
	 * The value of the selector.
	 * 
	 * @var string
	 */
	protected string $_value = '*';
	
	/**
	 * Builds a new CssAttributeContainsSelector with the given string values.
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function __construct(string $name, string $value = '*')
	{
		parent::__construct($name);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::__toString()
	 */
	public function __toString() : string
	{
		return '['.$this->getName().$this->getOperator().$this->getValue().']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::isAny()
	 */
	public function isAny() : bool
	{
		return '*' === $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAttributeSelectorInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelector::equals()
	 */
	public function equals($object) : bool
	{
		return parent::equals($object)
			&& $object instanceof CssAttributeContainsSelector
			&& $this->getValue() === $object->getValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::beVisitedBy()
	 */
	public function beVisitedBy(CssSelectorVisitorInterface $visitor)
	{
		return $visitor->visitAttributeSelector($this);
	}
	
}
