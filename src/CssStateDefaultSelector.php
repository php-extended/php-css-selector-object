<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use SplStack;

/**
 * CssStateDefaultSelector class file.
 * 
 * This class represents the :default pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateDefaultSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateDefaultSelector.
	 */
	public function __construct()
	{
		parent::__construct('default');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if('option' === $node->getName())
		{
			return $node->hasAttribute('selected');
		}
		
		if('button' === $node->getName())
		{
			// the default button is the first button in tree order, so check if we are the 1st
			if(null === $parentStack || $parentStack->isEmpty())
			{
				return false;
			}
			/** @var \PhpExtended\Html\HtmlCollectionNodeInterface $parent */
			$parent = $parentStack->top();
			
			return $parent->getFirstChild('button') === $node;
		}
		
		if('input' === $node->getName())
		{
			$attribute = $node->getAttribute('type');
			if(null === $attribute || !\in_array($attribute->getValue(), ['checkbox', 'radio'], true))
			{
				return false;
			}
			
			return $node->hasAttribute('checked');
		}
		
		return false;
	}
	
}
