<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use SplStack;

/**
 * CssStateIndeterminateSelector.
 * 
 * This class represents the :indeterminate pseudo-class selector.
 * 
 * @author Anastaszor
 */
class CssStateIndeterminateSelector extends CssAbstractStateSelector
{
	
	/**
	 * Builds a new CssStateIndeterminateSelector.
	 */
	public function __construct()
	{
		parent::__construct('indeterminate');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Css\CssAbstractSelectorInterface::matches()
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool
	{
		if('input' === $node->getName())
		{
			// inputs are undeterminate if they are all in the same state
			$attribute = $node->getAttribute('type');
			if(null === $attribute || !$attribute->valueEquals('radio'))
			{
				return false;
			}
			if(null === $parentStack || $parentStack->isEmpty())
			{
				return false;
			}
			$parent = $parentStack->top();
			
			return $this->matchesNode($parent);
		}
		
		if('progress' === $node->getName())
		{
			// progress are underterminate if they do not have value
			return !$node->hasAttribute('value');
		}
		
		return false;
	}
	
	/**
	 * Matches this css node with the parent html node.
	 * 
	 * @param HtmlCollectionNodeInterface $parent
	 * @return boolean
	 */
	public function matchesNode(HtmlCollectionNodeInterface $parent) : bool
	{
		$indeterminated = true;
		
		foreach($parent->getChildren('input') as $element)
		{
			$attribute = $element->getAttribute('type');
			if(null !== $attribute && $attribute->valueEquals('radio'))
			{
				$indeterminated = $indeterminated && $element->hasAttribute('checked');
			}
		}
		
		return (bool) $indeterminated;
	}
	
}
