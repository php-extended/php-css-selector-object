<?php declare(strict_types=1);

/**
 * Test script for css selector tag parser.
 * 
 * This file will give the data in object form.
 * 
 * Usage:
 * php test.php "<css_selector>"
 * 
 * @author Anastaszor
 */

use PhpExtended\Css\CssSelectorParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the css selector to parse.');
}
$data = $argv[1];

echo "\n\t".'Input : '.$data."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$parser = CssSelectorParser::get();
$node = $parser->parseString($data);
var_dump($node);
echo "\n\t".$node->__toString()."\n\n";
